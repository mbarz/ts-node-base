import { assert } from 'chai';
import {Greeter} from './greeter';

describe('example tests', () => {
    
    it('named greet', () => {
        let g = new Greeter().greet('Max');
        assert.equal(g, 'hallo Max!');
    });

    it('anominous greet', () => {
        let g = new Greeter().greet();
        assert.equal(g, 'hallo anominous user!');
    });

    it('anominous greet', () => {
        let g = new Greeter().greet();
        assert.equal(g, 'hallo anominous user!');
    });
});