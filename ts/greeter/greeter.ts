type MyType = string;

class Greeter {

    public greet(user?: string) {
        if (!user) user = 'anominous user';
        const greet = `hallo ${user}!`;
        console.log(greet);
        return greet;
    }
}

export { Greeter };