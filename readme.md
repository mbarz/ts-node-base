# setup for node app with typescript

## configuration

### types

typings (d.ts) are installed with npm with
```
npm install @types/... --save-dev
```

### compile

```
tsc -p ./
# or
npm run compile
# or in watch mode
tsc -p ./ -w
```

### tests

tests can be run with mocha or wallaby and are placed in the same folder as the code to test
```
mocha js/**/*.spec.js
# or
npm run test
# or in watch mode
mocha js/**/*.spec.js -w
```