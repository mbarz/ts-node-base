module.exports = function (wallaby) {

  return {
    files: [
      'ts/**/*.ts',
      { "pattern": "ts/**/*.spec.ts", "ignore": true },
      { "pattern": "ts/index.ts", "ignore": true }
    ],

    tests: [
      'ts/**/*.spec.ts'
    ],

    compilers: {
      '**/*.ts?(x)': wallaby.compilers.typeScript({
        module: 'commonjs',
        jsx: 'React'
      })
    },

    env: {
        type: 'node'
    }
  };
};